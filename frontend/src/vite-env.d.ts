/// <reference types="vite/client" />

interface ImportMetaEnv {
	readonly VITE_CHAT_WEBSOCKET: string;
}

interface ImportMeta {
	readonly env: ImportMetaEnv;
}
