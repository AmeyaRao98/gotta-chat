import { useKeyDownEvent } from "@solid-primitives/keyboard";
import { createWS } from "@solid-primitives/websocket";
import type { WSMessage } from "@solid-primitives/websocket";
import { Show } from "solid-js";
import type { Component } from "solid-js";
import { For, createEffect, createSignal } from "solid-js";
import { createStore } from "solid-js/store";
import styles from "./App.module.css";
const App: Component = () => {
	let ws:WebSocket
	
	const [messages, setMessages] = createStore<WSMessage[]>([]);
	
	const [chatJoined, setchatJoined] = createSignal<boolean>(false);
	const [messageInput, setMessageInput] = createSignal<string>("");
	
	function joinChat() {
		ws = createWS(import.meta.env.VITE_CHAT_WEBSOCKET);
		ws.addEventListener("message", (ev) => setMessages(messages.length, ev.data));
		setchatJoined(true);
	}

	const event = useKeyDownEvent();

	// handle input
	createEffect(() => {
		const e = event();

		if (e === null) {
			return;
		}
		switch (e.key) {
			case "Enter": {
				ws.send(messageInput());
				setMessageInput("");
				break;
			}
		}
	});
	return (
		<div class={styles.App}>
			<Show when={!chatJoined()}>
				<button type="button" class={styles.joinChat} onClick={joinChat}>
					Join chat
				</button>
			</Show>
			<Show when={chatJoined()}>
				<h1 class={styles.neonText}>Chat App</h1>
				<For each={messages}>
					{(message) => (
						<p class={`${styles.message} ${styles.fadeOut}`}>
							{message.toString()}
						</p>
					)}
				</For>
				<div class={styles.messageInput}>
					<input
						class={styles.field}
						maxlength="20"
						value={messageInput()}
						onInput={(e) => setMessageInput(e.target.value)}
					/>
				</div>
			</Show>
		</div>
	);
};

export default App;
