Universal chat application, where a message sent by a connected client is broadcast to every other connected client
This is done by allowing a client to connect to the backend via a websocket, and any message received by a websocket is sent to every active websocket connection

Backend written in sanic
Frontend written in svelte

backend: http://localhost:7000
frontend: http://localhost:5173


https://gotta-chat-frontend.onrender.com/