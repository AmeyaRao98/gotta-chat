import asyncio

from pydantic_settings import BaseSettings
from sanic import Request, Sanic, Websocket
from sanic.exceptions import ServerError, WebsocketClosed
from sanic.response import text
from websockets.typing import Data


class Settings(BaseSettings):
    sanic_host: str = "127.0.0.1"
    sanic_port: int = 7000
    sanic_fast: bool = False

    dev: bool = True


settings = Settings()

app = Sanic("gotta-chat")

connected_clients: set[Websocket] = set()


async def handle_message(ws: Websocket, message: Data):
    try:
        await ws.send(message)
    except (WebsocketClosed, ServerError):
        connected_clients.remove(ws)


@app.get("/health")
async def healthcheck(request: Request):
    return text("skrrrrrrrrrrrrrrt")


@app.websocket("/chat")
async def chat(request: Request, ws: Websocket):
    connected_clients.add(ws)
    async for message in ws:
        if message is not None:
            await asyncio.gather(
                *(handle_message(w, message) for w in connected_clients)
            )


if __name__ == "__main__":
    app.run(
        host=settings.sanic_host,
        port=settings.sanic_port,
        debug=settings.dev,
        auto_reload=settings.dev,
        fast=settings.sanic_fast,
    )
