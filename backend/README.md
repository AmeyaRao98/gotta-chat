Ensure that uv is installed:

https://docs.astral.sh/uv/getting-started/installation/

Install dependencies:

`uv sync`

Run project on http://localhost:8000:

`make run`


Linting/formatting:

```
make lint
```